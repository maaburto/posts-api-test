'use strict';

const express = require('express');
const router = express.Router();
const Authenticate = require('../controllers/authenticate');

router['route']('/').post(Authenticate);

module.exports = router;
