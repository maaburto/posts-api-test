'use strict';

const express = require('express');
const router = express.Router();
const {
  findAll,
  insert,
  findOne,
  update,
  remove
} = require('../controllers/posts');

router['route']('/').get(findAll);
router['route']('/').post(insert);
router['route']('/:id').get(findOne)
router['route']('/:id').put(update)
router['route']('/:id').delete(remove)

module.exports = router
