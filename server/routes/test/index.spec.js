const chai = require('chai');
const Routes = require('../index').Routes;
const mockPackageJSON = {
  name: 'posts-api',
  version: '0.0.1',
};

beforeAll(async () => {
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Index Route suite', () => {
  it('Should returns mockPackageJSON input payload', () => {
    const routes = new Routes(mockPackageJSON);
    const {
      jsonp: {
        name, version
      }
    } = routes;
    const {
      name: assertName,
      version: assertVersion
    } = mockPackageJSON;

    name.should.be.equals(assertName);
    version.should.be.equals(assertVersion);
  });

  it('Should returns router function', () => {
    const routes = new Routes(mockPackageJSON);
    const welcome = routes.indexRoute();
    const route = routes.route();
    
    welcome.should.be.a('function');
    route.should.be.a('function');
  });
});

