const chai = require('chai');
const { getRouting } = require('./util');
const router = require('../users');

beforeAll(async () => {
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Users Route suite', () => {
  it('Should returns an router object', () => {
    const { route } = getRouting(router, 'post', '/');
    route.should.be.a('object');    
  });

  it('Should returns a \"post\" as method and \"/\" as path', () => {
    const {
      route: {
        path,
        methods
      }
    } = getRouting(router, 'post', '/');
    
    methods.should.be.a('object');
    methods.should.have.property('post').that.has.equals(true);
    path.should.be.equals('/');
  });
});

