const chai = require('chai');
const { getRouting } = require('./util');
const router = require('../posts');

beforeAll(async () => {
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Posts Route suite', () => {
  it('Should returns an router object at POST request', () => {
    const { route } = getRouting(router, 'post', '/');
    route.should.be.a('object');    
  });

  it('Should returns a \"post\" as method and \"/\" as path', () => {
    const {
      route: {
        path,
        methods
      }
    } = getRouting(router, 'post', '/');
    
    methods.should.be.a('object');
    methods.should.have.property('post').that.has.equals(true);
    path.should.be.equals('/');
  });

  it('Should returns an router object at GET request', () => {
    const { route } = getRouting(router, 'get', '/');
    route.should.be.a('object');    
  });

  it('Should returns a \"get\" as method and \"/\" as path', () => {
    const {
      route: {
        path,
        methods
      }
    } = getRouting(router, 'get', '/');
    
    methods.should.be.a('object');
    methods.should.have.property('get').that.has.equals(true);
    path.should.be.equals('/');
  });

  it('Should returns an router object at GET with params', () => {
    const { route } = getRouting(router, 'get', '/:id');
    route.should.be.a('object');    
  });

  it('Should returns a \"get\" as method and \"/\" as path along params', () => {
    const {
      route: {
        path,
        methods
      }
    } = getRouting(router, 'get', '/:id');

    methods.should.be.a('object');
    methods.should.have.property('get').that.has.equals(true);
    path.should.be.equals('/:id');
  });

  it('Should returns an router object at PUT with params', () => {
    const { route } = getRouting(router, 'put', '/:id');
    route.should.be.a('object');    
  });

  it('Should returns a \"put\" as method and \"/\" as path along params', () => {
    const {
      route: {
        path,
        methods
      }
    } = getRouting(router, 'put', '/:id');

    methods.should.be.a('object');
    methods.should.have.property('put').that.has.equals(true);
    path.should.be.equals('/:id');
  });

  it('Should returns an router object at DELETE with params', () => {
    const { route } = getRouting(router, 'delete', '/:id');
    route.should.be.a('object');    
  });

  it('Should returns a \"delete\" as method and \"/\" as path along params', () => {
    const {
      route: {
        path,
        methods
      }
    } = getRouting(router, 'delete', '/:id');

    methods.should.be.a('object');
    methods.should.have.property('delete').that.has.equals(true);
    path.should.be.equals('/:id');
  });
});

