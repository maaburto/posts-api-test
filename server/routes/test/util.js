const getRouting = (router, method, URPath) => {
  return router.stack.find((
    {
      route: { methods, path }
    }) => {
    return methods[method] === true &&  path === URPath
  });
};

module.exports = { getRouting };