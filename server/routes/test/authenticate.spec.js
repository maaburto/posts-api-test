const chai = require('chai');
const { getRouting } = require('./util');
const router = require('../authenticate');

beforeAll(async () => {
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Authenticate Route suite', () => {
  it('Should returns an router object', () => {
    const { route } = getRouting(router, 'post', '/');
    const { path, methods } = route;
    
    route.should.be.a('object');
    methods.should.be.a('object');
    methods.should.have.property('post');
    path.should.be.equals('/');
  });
});

