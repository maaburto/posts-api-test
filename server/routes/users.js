'use strict';

const express = require('express');
const router = express.Router();
const { insert } = require('../controllers/users');

// Insert an User
router['route']('/').post(insert);

module.exports = router;
