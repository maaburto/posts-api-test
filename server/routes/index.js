'use strict';

const express = require('express');
const router = express.Router();
const Users = require('../controllers/users');
const Auth = require('../middlewares/authProvider');

function Routes(JSONPackage) {
  this.jsonp = JSONPackage;
  router['use']('/users', Auth, require('./users'));
  router['use']('/posts', Auth, require('./posts'));
  router['use']('/login', require('./authenticate'));
  router['route']('/signup').post(Users.insert);
}

function indexRoute() {
  const { name, version } = this.jsonp;
  const response = {
    'name': name,
    'version': version
  };

  return router['get']('/', (req, res) => {
    res.json(response);
  });
}

function route() {
  return router;
}

Routes.prototype = { route, indexRoute };
module.exports = { Routes };
