'use strict';

const { connectionString } = require('../../config');
require('../utils/db').instance(connectionString);

const mongoose = require('mongoose');
const config = require('../../config');
const users = require('../schemas/users');
const posts = require('../schemas/posts');

class Models {
  constructor() {
    this.Users = mongoose.model('users', users);
    this.Posts = mongoose.model('posts', posts);
  }

  getUsersModelSchema() {  
    return this.Users;
  }

  getPostsModelSchema() {
    return this.Posts;
  }

  getConfig(env) {
    return config[env];
  }
}

module.exports = Models;
