'use strict';

const mongoose = require('mongoose');
const Users = require('./users');
const Models = require('./index');
const model = new Models();

async function findAll() {
  try {
    const dbo = model.getPostsModelSchema();
    const rows = await dbo.find();
    return rows
  } catch(err) {
    throw err;
  }
}

async function insert(collection) {
  try {
    const dbo = model.getPostsModelSchema();
    const post = await dbo.create(collection);
    const { creator } = post;
    
    const user = await Users.findById(creator);
    user.posts.push(post);
    await Users.updateOne(user);

    return post;
  } catch(err) {
    throw err;
  }
}

async function findById(id) {
  try {
    const dbo = model.getPostsModelSchema();
    const { Types: {
      ObjectId
      }
    } = mongoose;
    const post = await dbo.findOne({ _id: new ObjectId(id) });
    return post;
  } catch(err) {
    throw err;
  }
}

async function update(row) {
  const dbo = model.getPostsModelSchema();
  try {
    const post = await dbo.updateOne(row);
    return post;
  } catch(err) {
    throw err;
  }
}

async function remove(_id) {
  const dbo = model.getPostsModelSchema();
  try {
    const postSelected = await findById(_id);
    let post = null;

    if (postSelected !== null) {
      const { creator } = postSelected;
      post = await dbo.remove({ _id });
      const { deletedCount } = post;

      if (deletedCount === 1) {
        const user = await Users.findById(creator);
        const newPosts = user.posts.filter((
          {
            _id: postId
          }) => String(postId) !== _id);
        user.posts = newPosts;
        await Users.updateOne(user);
      }
    }

    return post;
  } catch(err) {
    throw err;
  }
}

module.exports = { findAll, findById, insert, update, remove }
