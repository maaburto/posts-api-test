'use strict';

const Models = require('./index');
const model = new Models();
const { hashPassword } = require('../utils/crypto');
const SALT_WORK_FACTOR = model.getConfig('saltFactor');

async function insert(collection) {
  try {
    const dbo = model.getUsersModelSchema();
    collection.password = await hashPassword(SALT_WORK_FACTOR, collection.password);
    const user = await dbo.create(collection);
    return user;
  } catch(err) {
    throw err;
  }
}

async function findOne(criterias) {
  try {
    const dbo = model.getUsersModelSchema();
    const user = await dbo.findOne(criterias);
    return user;
  } catch(err) {
    throw err;
  }
}

async function findById(id) {
  try {
    const dbo = model.getUsersModelSchema();
    const user = await dbo.findById({ _id: id });
    return user;
  } catch(err) {
    throw err;
  }
}

async function updateOne(row) {
  try {
    const dbo = model.getUsersModelSchema();
    const user = await dbo.updateOne(row);
    return user;
  } catch(err) {
    throw err;
  }
}

module.exports = { insert, findOne, findById, updateOne };
