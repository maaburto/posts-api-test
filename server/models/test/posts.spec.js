const chai = require('chai');
const { MockModelClass, creatorPostId, mockNewPost } = require('./utils');
const MockSchemaModel = require('../index');
const mockUserSchemaModel = require('../users');
const { findAll, insert, remove } = require('../posts');

jest.mock('../index', () => {
  return jest.fn().mockImplementation(() => MockModelClass());
});

jest.mock('../users', () => {
  return {
    findById: jest.fn().mockReturnValue({
      _id: '5dd05cef5d8e950100617cf6',
      name: 'Jhon',
      email: 'Jhon.Doe@blade.com',
      posts: [],
    }),
    updateOne: jest.fn()
  }
});

beforeEach(() => {
  MockSchemaModel.mockClear();
});

beforeAll(() => {
  global.postPayload = {
    title: 'Magic Post',
    imageUrl: 'https://www.fake.com',
    content: 'My Magic Content',
    creator: '5dd05cef5d8e950100617cf6'
  };
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Post Model suite', () => {
  it('Should have called create method and push new posts into user model Schema', async () => {
    const mockSchemaModel = new MockSchemaModel();
    const mockPost = mockSchemaModel.getPostsModelSchema();
    await insert(postPayload);
    mockPost.create(postPayload);
    jestExpect(mockPost.create).toHaveBeenCalled();
    jestExpect(mockPost.create).toHaveReturnedTimes(1);
    jestExpect(mockUserSchemaModel.findById).toHaveBeenCalled();
    jestExpect(mockUserSchemaModel.findById).toHaveBeenCalledWith(creatorPostId);
    jestExpect(mockUserSchemaModel.updateOne).toHaveBeenCalled();
    jestExpect(mockUserSchemaModel.updateOne).toHaveReturned();
  });

  it('Should have called findAll method', async () => {
    const mockSchemaModel = new MockSchemaModel();
    const mockPost = mockSchemaModel.getPostsModelSchema();
    await findAll();
    mockPost.find();
    jestExpect(mockPost.find).toHaveBeenCalled();
    jestExpect(mockPost.find).toHaveReturnedTimes(1);
  });

  it('Should have called remove method', async () => {
    const mockSchemaModel = new MockSchemaModel();
    const mockPost = mockSchemaModel.getPostsModelSchema();
    await remove();
    mockPost.remove();
    jestExpect(mockPost.remove).toHaveBeenCalled();
    jestExpect(mockPost.remove).toHaveReturnedTimes(1);
  });
});
