const creatorPostId = '5dd05cef5d8e950100617cf6';
const mockNewPost = {
  _id: '5dd1e63af738b2423009a9fc',
  creator: creatorPostId
};
const MockModelClass = () => {
  const mockFoundPost = mockNewPost;
  return {
    getConfig: () => 10,
    getUsersModelSchema: () => ({
      create: jest.fn(),
      findOne: jest.fn(),
      updateOne: jest.fn(),
    }),
    getPostsModelSchema: () => ({
      create: jest.fn().mockReturnValue(mockNewPost),
      find: jest.fn(),
      findOne: jest.fn().mockReturnValue(mockFoundPost),
      remove: jest.fn().mockReturnValue({
        n: 1,
        ok: 1,
        deletedCount: 1
      }),
    })
  };
};

module.exports = { MockModelClass, creatorPostId, mockNewPost };
