const chai = require('chai');
const MockSchemaModel = require('../index');
const { MockModelClass } = require('./utils');
const { insert, findOne, updateOne } = require('../users');

jest.mock('../index', () => {
  return jest.fn().mockImplementation(() => MockModelClass());
});

beforeEach(() => {
  MockSchemaModel.mockClear();
});

beforeAll(() => {
  global.userPayload = {
    name: 'Jhon',
    email: 'Jhon.Doe@blade.com',
    password: 'password123'
  };
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('User Model suite', () => {
  it('Should have called create method', async () => {
    const mockSchemaModel = new MockSchemaModel();
    const mockUser = mockSchemaModel.getUsersModelSchema();
    await insert(userPayload);
    mockUser.create(userPayload);
    const { password } = userPayload;
    jestExpect(mockUser.create).toHaveBeenCalled();
    jestExpect(mockUser.create).toHaveReturnedTimes(1);
    jestExpect(password).toMatch('$2b$10');
  });

  it('Should have called findOne method', async () => {
    const mockSchemaModel = new MockSchemaModel();
    const mockUser = mockSchemaModel.getUsersModelSchema();
    const criteria = {
      email: 'fake@email.com' 
    };
    await findOne(criteria);
    mockUser.findOne(criteria);

    jestExpect(mockUser.findOne).toHaveBeenCalled();
    jestExpect(mockUser.findOne).toHaveReturnedTimes(1);
    jestExpect(mockUser.findOne).toHaveBeenCalledWith(criteria);
  });

  it('Should have called updateOne method', async () => {
    const mockSchemaModel = new MockSchemaModel();
    const mockUser = mockSchemaModel.getUsersModelSchema();
    const payload = {
      id: 'fakeObjectId',
      ...userPayload
    };
    await updateOne(payload);
    mockUser.updateOne(payload);

    jestExpect(mockUser.updateOne).toHaveBeenCalled();
    jestExpect(mockUser.updateOne).toHaveReturnedTimes(1);
  });
});
