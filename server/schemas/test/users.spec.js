const mongoose = require('mongoose');
const chai = require('chai');
const Schema = require('../users');

beforeAll(async () => {
  Model = mongoose.model('users', Schema);
  global.model = new Model();
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Users Schema suite', () => {
  it('Should has email validation', (done) => {
    model.validate((error) => {
      const {
        errors: {
          email: { message, name }
        }
      } = error;
      error.should.be.a('object');
      message.should.be.a('string');
      name.should.be.equals('ValidatorError');
      done();
    });
  });

  it('Should have a defined schema', () => {
    const {
      Schema: {
        Types: {
          String, Array, Date
        }
      }
    } = mongoose;
    const name = Schema.path('name');
    const email = Schema.path('email');
    const password = Schema.path('password');
    const posts = Schema.path('posts');
    const date =  Schema.path('date');

    name.should.be.an.instanceof(String);
    email.should.be.an.instanceof(String);
    password.should.be.an.instanceof(String);
    posts.should.be.an.instanceOf(Array);
    date.should.be.an.instanceOf(Date);
  });  
});
