require('mongodb');
const mongoose = require('mongoose');
const chai = require('chai');
const Schema = require('../../posts');
let newPost = {};

beforeAll(async () => {
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Posts ModelSchema suite', () => {
  // It's just so easy to connect to the MongoDB Memory Server 
  // By using mongoose.connect
  beforeAll(async () => {
    const opts = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true
    };
    const {
      env: {
        MONGO_URL
      }
    } = process;
    await mongoose.connect(MONGO_URL, opts, (err) => {
      if (err) {
        console.error(err);
      }
    });

    global.model = mongoose.model('posts', Schema);
  });

  it('Should create & save posts successfully', async () => {
    const data = {
      title: 'Magic Post',
      imageUrl: 'https://www.fake.com',
      content: 'My Magic Content',
      creator: '5dd05cef5d8e950100617cf6'
    };
    const post = await model.create(data);
    newPost = post;
    // Object Id should be defined when successfully saved to MongoDB.
    post.should.be.exist;
    post._id.should.be.a('object');
    post.title.should.be.a('string');
    post.imageUrl.should.be.a('string');
    post.content.should.be.a('string');
    post.creator.should.be.a('object');
    post.date.should.be.a('date');
  });

  it('Should get one item from findById function', async () => {
    const _id = new String(newPost._id);

    const post = await model.findById(_id);
    post.should.be.exist;
    post.title.should.be.equal('Magic Post');
  });

  it('Should delete one item from remove function', async () => {
    const { _id } = newPost;

    const { deletedCount } = await model.remove({ _id });
    deletedCount.should.be.exist;
    deletedCount.should.be.equal(1);
  });

  afterAll(async () => {
    const collections = Object.keys(mongoose.connection.collections);
    for (const collectionName of collections) {
      const collection = mongoose.connection.collections[collectionName];
      await collection.deleteMany();
    }
  });
});
