require('mongodb');
const mongoose = require('mongoose');
const chai = require('chai');
const Schema = require('../../users');

beforeAll(async () => {
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Users ModelSchema suite', () => {
  // It's just so easy to connect to the MongoDB Memory Server 
  // By using mongoose.connect
  beforeAll(async () => {
    const opts = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true
    };
    const {
      env: {
        MONGO_URL
      }
    } = process;
    await mongoose.connect(MONGO_URL, opts, (err) => {
      if (err) {
        console.error(err);
      }
    });

    global.model = mongoose.model('users', Schema);
  });

  it('Should create & save user successfully', async () => {
    const data = {
      name: 'Jhon',
      email: 'Jhon.Doe@blade.com',
      password: 'password123'
    };
    const user = await model.create(data);
    // Object Id should be defined when successfully saved to MongoDB.
    user.should.be.exist;
    user._id.should.be.a('object');
    user.name.should.be.a('string');
    user.email.should.be.a('string');
    user.posts.should.be.a('array');
    user.date.should.be.a('date');
  });

  it('Should not create user because is not a valid email', async () => {
    let err;
    const data = {
      name: 'Jhon',
      email: 'Jhon.Doe'
    };
    try {
      const user = await model.create(data);
      error = user;
    } catch (error) {
      err = error
    }

    err.errors.email.should.exist;
    (err.errors.email.name).should.be.equal('ValidatorError');
  });

  it('Should get one item from findOne function', async () => {
    const email = 'jhon.doe@blade.com';
    const user = await model.findOne({ email });
    
    user.should.be.exist;
    user.email.should.be.equal(email);
  });

  afterAll(async () => {
    const collections = Object.keys(mongoose.connection.collections);
    for (const collectionName of collections) {
      const collection = mongoose.connection.collections[collectionName];
      await collection.deleteMany();
    }
  });
});
