const mongoose = require('mongoose');
const chai = require('chai');
const Schema = require('../posts');

beforeAll(async () => {
  Model = mongoose.model('posts', Schema);
  global.model = new Model();
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Posts Schema suite', () => {
  it('Should have a defined schema', () => {
    const {
      Schema: {
        Types: {
          String, ObjectId, Date
        }
      }
    } = mongoose;
    const title = Schema.path('title');
    const imageUrl = Schema.path('imageUrl');
    const content = Schema.path('content');
    const creator = Schema.path('creator');
    const date =  Schema.path('date');

    title.should.be.an.instanceof(String);
    imageUrl.should.be.an.instanceof(String);
    content.should.be.an.instanceof(String);
    creator.should.be.an.instanceOf(ObjectId);
    date.should.be.an.instanceOf(Date);
  });  
});
