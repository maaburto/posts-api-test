'use strict';

const SchemaInstance = require('./index').Schemas;
const { versionKey } = new SchemaInstance();
const options = {
  ...versionKey
};
const mongoose = require('mongoose');
const {
  Schema: {
    Types: { ObjectId }
  }
} = mongoose;
const Schema = mongoose.Schema;

const validateEmail = (email) => {
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email)
};

const schema = new Schema({
  name : String,
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: 'Email address is required',
    validate: [validateEmail, 'Please fill a valid email address'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
  },
  password: String,
  posts: [{ _id : ObjectId }],
  date: { type: Date, default: Date.now }
}, options);


module.exports = schema;
