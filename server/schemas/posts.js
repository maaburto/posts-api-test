'use strict';

const SchemaInstance = require('./index').Schemas;
const { versionKey } = new SchemaInstance();
const options = {
  ...versionKey
};
const mongoose = require('mongoose');
const {
  Schema: {
    Types: { ObjectId }
  }
} = mongoose;
const Schema = mongoose.Schema;

const schema = new Schema({
  title : String,
  imageUrl: String,
  content: String,
  creator: ObjectId,
  date: { type: Date, default: Date.now }
}, options);

module.exports = schema;
