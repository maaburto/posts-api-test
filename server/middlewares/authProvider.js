'use strict';

const JWT = require('../utils/crypto');
const config = require('../../config');
const AllowedEndpoints = require('../utils/allowedEndpoints');

module.exports = verify;

async function verify(req, res, next) {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];
  const url = req.originalUrl;
  const method = req.method;
  const noTokenMessage = {
    success: false,
    message: 'No token provided.'
  };
  const failedAuthMessage = {
    success: false,
    message: 'Failed to authenticate token.'
  };
  const secret = req.app.get('superSecret');

  if(AllowedEndpoints(url, method)) {
    return next();
  }

  if (!token) {
    // if there is no token
    // return an error
    return res.status(401).send(noTokenMessage);
  }

  try {
    if (token === config.token) {
      req.decoded = {
        user_id: config.apiUserId
      };
      next();
    } else {
      const decoded = await JWT.verify(token, secret);
      // if everything is good, save to request for use in other routes
      req.decoded = decoded.user;
      next();
    }
  } catch (err) {
    if (err.name === 'JsonWebTokenError') {
      return res.status(401).json(failedAuthMessage);
    } else {
      throw err;
    }
  }
}
