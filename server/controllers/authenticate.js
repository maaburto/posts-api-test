'use strict';

const Controllers = require('./index').Controllers;
const controller = new Controllers();
const config = controller.getConfig();

const { comparePassword, sign } = require('../utils/crypto');
const { findOne } = require('../models/users');

module.exports = async (req, res, next) => {
  const { email, password } = req.body;
  const badInput = {
    'message': 'Bad Input'
  };

  if (!(email && password)) {
    return res.status(400).json({
      'message': 'Missing properties'
    });
  }

  try {
    const superSecret = req.app.get('superSecret');
    const foundOneUser = await findOne({ email });
    let user = {};

    if (!foundOneUser) {
      if (foundOneUser === null) {
        return res.status(400).json(badInput);
      }
    }

    user = foundOneUser._doc;
    const comparePass = await comparePassword(password, user.password);
    const jwtExpire = config['jwtExpire'];

    if (comparePass === true) {
      const token = await sign(user['_id'], superSecret, jwtExpire);
      user.token = token;
      delete user.password;
      delete user.posts;

      return res.status(200).json(user);
    } else {
      return res.status(400).json(badInput);
    }
  } catch (err) {
    return next(err);
  }
};
