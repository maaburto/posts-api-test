'use strict';

const config = require('../../config');

function Controllers() {}

function getConfig() {
  return config;
}

Controllers.prototype = { getConfig };

module.exports = { Controllers };
