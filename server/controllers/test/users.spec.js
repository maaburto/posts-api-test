const chai = require('chai');
const { mockRequest, mockResponse, mockNext } = require('./utils');
const mockModel = require('../../models/users');
const { insert } = require('../users');

jest.mock('../../models/users');

beforeAll(() => {
  global.userPayload = {
    name: 'Jhon',
    email: 'Jhon.Doe@blade.com',
    password: 'password123'
  };
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('User Controller suite', () => {
  it('Should have called insert method', async () => {
    const body = userPayload;
    const req = mockRequest(body);
    const res = mockResponse();
    const next = mockNext;

    await insert(req, res, next);
    jestExpect(mockModel.insert).toBeCalled();  
  });

  it('Should have called json method', async () => {
    const body = userPayload;
    const req = mockRequest(body);
    const res = mockResponse();
    const next = mockNext;

    await insert(req, res, next);
    jestExpect(res.json).toHaveBeenCalled();
  });
});
