const chai = require('chai');
const mockCryptoModel = require('../../utils/crypto');
const { mockRequest, mockResponse, mockNext } = require('./utils');
const mockModel = require('../../models/users');
const auth = require('../authenticate');
const payloadAuthenticate = {
  email: 'Jhon.Doe@blade.com',
  password: 'password123'
};
const findOneResult = {
  _doc: {
    _id: '5dd05cef5d8e950100617cf6',
    name: 'Jhon',
    email: 'Jhon.Doe@blade.com'
  }
};

jest.mock('../../models/users');
jest.mock('../../utils/crypto');

beforeAll(async () => {
  mockCryptoModel.comparePassword.mockReturnValue(true);
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Authenticate Controller suite', () => {
  it('Should login without problem and return a 200 HTTP status code', async () => {
    const body = payloadAuthenticate;
    const req = mockRequest(body);
    const res = mockResponse();
    const next = mockNext;

    mockModel.findOne.mockReturnValue(findOneResult);
    await auth(req, res, next);
    jestExpect(res.json).toBeCalled();
    jestExpect(res.status).toHaveBeenCalledWith(200);
  });


  it('Should login without problem and invoke sign function', async () => {
    const body = payloadAuthenticate;
    const req = mockRequest(body);
    const res = mockResponse();
    const next = mockNext;

    mockModel.findOne.mockReturnValue(findOneResult);
    await auth(req, res, next);
    jestExpect(mockCryptoModel.sign).toBeCalled();
  });

  it('Should throw 400 HTTP status code because there is no email or password', async () => {
    const body = {};
    const req = mockRequest(body);
    const res = mockResponse();
    const next = mockNext;

    mockModel.findOne.mockReturnValue(findOneResult);
    await auth(req, res, next);
    jestExpect(res.json).toBeCalled();
    jestExpect(res.status).toHaveBeenCalledWith(400);

    body.email = payloadAuthenticate.email;
    jestExpect(res.status).toHaveBeenCalledWith(400);
  });

  it('Should throw 400 HTTP status code because there is not a user found', async () => {
    const body = payloadAuthenticate;
    const req = mockRequest(body);
    const res = mockResponse();
    const next = mockNext;
    let findOneData = null;

    mockModel.findOne.mockReturnValue(findOneData);
    await auth(req, res, next);
    jestExpect(res.json).toBeCalled();
    jestExpect(res.status).toHaveBeenCalledWith(400);

    mockModel.findOne.mockReturnValue(findOneData);
    await auth(req, res, next);
    jestExpect(res.status).toHaveBeenCalledWith(400);
  });

  it('Should throw 400 HTTP status code because password doesn\'t match', async () => {
    const body = {};
    const req = mockRequest(body);
    const res = mockResponse();
    const next = mockNext;

    mockCryptoModel.comparePassword.mockReturnValue(false);
    mockModel.findOne.mockReturnValue(findOneResult);
    await auth(req, res, next);
    jestExpect(res.json).toBeCalled();
    jestExpect(res.status).toHaveBeenCalledWith(400);
  });

  afterEach(() => {
    mockCryptoModel.comparePassword.mockClear();
    mockModel.findOne.mockClear();
  });
});
