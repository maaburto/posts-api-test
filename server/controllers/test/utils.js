const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);

  return res;
};

const mockRequest = (body = {}, params = {}, sessionData) => {
  return {
    app: {
      get: (key) => {
        switch (key) {
          case 'superSecret':
            return 'superSecret';
          break;

          default:
           return null;
        }
      }
    },
    session: { data: sessionData },
    body,
    params
  };
};

const mockNext = () => { };

module.exports = { mockRequest, mockResponse, mockNext };
