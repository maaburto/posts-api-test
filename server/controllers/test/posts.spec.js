const chai = require('chai');
const { mockRequest, mockResponse, mockNext } = require('./utils');
const mockModel = require('../../models/posts');
const { insert, findAll, findOne, update, remove } = require('../posts');

jest.mock('../../models/posts');

beforeAll(() => {
  global.params = {
    id: '5dd05cef5d8e950100617cf6'
  };
  global.postPayload = {
    "title": "Magic Post",
    "imageUrl": "https://www.fake.com",
    "content": "My Magic Content",
    "creator": "5dd05cef5d8e950100617cf6"
  };
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('Posts Controller suite', () => {
  it('Should have called insert function', async () => {
    const body = postPayload;
    const req = mockRequest(body);
    const res = mockResponse();
    const next = mockNext;

    await insert(req, res, next);
    jestExpect(mockModel.insert).toBeCalled();  
  });

  it('Should have called json function', async () => {
    const body = postPayload;
    const req = mockRequest(body);
    const res = mockResponse();
    const next = mockNext;

    await insert(req, res, next);
    jestExpect(res.json).toHaveBeenCalled();
  });

  it('Should have called status function and return 200', async () => {
    const body = postPayload;
    const req = mockRequest(body);
    const res = mockResponse();
    const next = mockNext;

    await insert(req, res, next);
    jestExpect(res.status).toHaveBeenCalledWith(200);
  });

  it('Should have called findAll model and json function', async () => {
    const req = mockRequest();
    const res = mockResponse();
    const next = mockNext;

    await findAll(req, res, next);
    jestExpect(mockModel.findAll).toBeCalled();
    jestExpect(res.json).toHaveBeenCalled();
  });

  it('Should have called findAll model and json function', async () => {
    const req = mockRequest();
    const res = mockResponse();
    const next = mockNext;

    await findAll(req, res, next);
    jestExpect(mockModel.findAll).toBeCalled();
    jestExpect(res.json).toHaveBeenCalled();
  });

  it('Should have called findAll model and return 200', async () => {
    const req = mockRequest();
    const res = mockResponse();
    const next = mockNext;

    await findAll(req, res, next);
    jestExpect(mockModel.findAll).toBeCalled();
    jestExpect(res.status).toHaveBeenCalledWith(200);
  });

  it('Should have called findOne model and json function', async () => {
    const body = {};
    const req = mockRequest(body, params);
    const res = mockResponse();
    const next = mockNext;

    await findOne(req, res, next);
    jestExpect(mockModel.findById).toBeCalled();
    jestExpect(res.json).toHaveBeenCalled();
  });

  it('Should have called findOne model and return 200', async () => {
    const body = {};
    const req = mockRequest(body, params);
    const res = mockResponse();
    const next = mockNext;

    await findOne(req, res, next);
    jestExpect(res.status).toHaveBeenCalledWith(200);
  });

  it('Should have called update model and json function', async () => {
    const body = postPayload;
    const req = mockRequest(body, params);
    const res = mockResponse();
    const next = mockNext;

    await update(req, res, next);
    jestExpect(mockModel.update).toBeCalled();
    jestExpect(res.json).toHaveBeenCalled();
  });

  it('Should have same payload to send it to update model function', async () => {
    const body = postPayload;
    const req = mockRequest(body, params);
    const res = mockResponse();
    const next = mockNext;

    const assertPayload = {
      id: params.id,
      ...body
    };

    await update(req, res, next);
    jestExpect(mockModel.update).toHaveBeenCalledWith(assertPayload);
  });

  it('Should have called update model function and return 200', async () => {
    const body = postPayload;
    const req = mockRequest(body, params);
    const res = mockResponse();
    const next = mockNext;

    await update(req, res, next);
    jestExpect(res.status).toHaveBeenCalledWith(200);
  });

  it('Should have called remove model and json function', async () => {
    const body = {};
    const req = mockRequest(body, params);
    const res = mockResponse();
    const next = mockNext;

    await remove(req, res, next);
    jestExpect(mockModel.remove).toBeCalled();
    jestExpect(res.json).toHaveBeenCalled();
  });

  it('Should not have same id to send it to remove model function', async () => {
    const body = {};
    const parameters = {
      fake: true,
      ...params
    };
    const { id } = params;
    const req = mockRequest(body, parameters);
    const res = mockResponse();
    const next = mockNext;

    await remove(req, res, next);
    jestExpect(mockModel.remove).toHaveBeenCalledWith(id);
  });

  it('Should have called remove model function and return 200', async () => {
    const body = {};
    const req = mockRequest(body, params);
    const res = mockResponse();
    const next = mockNext;

    await remove(req, res, next);
    jestExpect(res.status).toHaveBeenCalledWith(200);
  });
});
