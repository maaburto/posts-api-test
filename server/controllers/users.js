'use strict';

const User = require('../models/users');

module.exports = { insert };

async function insert(req, res, next) {
  try {
    const { body } = req;
    const data = await User.insert(body);

    return res.json(data);
  } catch (err) {
    return next(err);
  }
}
