'use strict';

const Posts = require('../models/posts')

async function findAll(req, res, next) {
  try {
    const data = await Posts.findAll();
    return res.status(200).json(data);
  } catch (err) {
    return next(err);
  }
}

async function findOne(req, res, next) {
  try {
    const {
      params: { id }
    } = req;
    const data = await Posts.findById(id);

    return res.status(200).json(data);
  } catch (err) {
    return next(err)
  }
}

async function insert(req, res, next) {
  try {
    const { body } = req;
    const data = await Posts.insert(body);
    return res.status(200).json(data)
  } catch (err) {
    return next(err)
  }
}

async function update(req, res, next) {
  try {
    const {
      params: { id },
      body
    } = req;
    const row = {
      id,
      ...body
    };
    const data = await Posts.update(row);
    return res.status(200).json(data);
  } catch (err) {
    return next(err)
  }
}

async function remove(req, res, next) {
  try {
    const {
      params: { id }
    } = req;
    const data = await Posts.remove(id);
    return res.status(200).json(data);
  } catch (err) {
    return next(err)
  }
}

module.exports = { findAll, findOne, insert, update, remove }
