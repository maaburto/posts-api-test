'use strict';

const mongoose = require('mongoose');

// DB Stuff
function instance(connectionString) {
  const opts = {
    useNewUrlParser: true,
    useUnifiedTopology: true
  };

  mongoose.connect(connectionString, opts);
  return mongoose.connection;
}

module.exports = { instance };
