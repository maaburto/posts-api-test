'use strict';

const Promise = require('bluebird');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

Promise.promisifyAll(bcrypt);
Promise.promisifyAll(jwt);

module.exports = { hashPassword, comparePassword, sign, verify };

async function hashPassword(saltFactor, password) {
  try {
    const salt = await bcrypt['genSaltAsync'](saltFactor);
    const hash = await bcrypt['hashAsync'](password, salt);

    return hash;
  } catch (err) {
    throw err;
  }
}

async function comparePassword(password, hash) {
  try {
    const result = await bcrypt['compareAsync'](password, hash);
    return result;
  } catch (err) {
    throw err;
  }
}

async function sign(userId, superSecret, expiresIn) {
  const payload = { userId };
  const expire = { expiresIn };
  try {
    const token = await jwt['signAsync'](payload, superSecret, expire);
    return token;
  } catch (err) {
    throw err;
  }
}

async function verify(token, superSecret) {
  try {
    // verifies secret and checks exp
    const decoded = jwt['verifyAsync'](token, superSecret);

    return decoded;
  } catch (err) {
    throw err;
  }
}
