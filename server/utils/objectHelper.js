'use strict';

const changeCase = require('change-case');

function UpdateKeys(payload) {
  this.myPayload = payload;

  this.snakeCase = function() {
    let final;
    let updated = {};
    let updatedArray = [];
    var original = this.myPayload;


    if (Array.isArray(original)) {
      let originalLength = original.length;

      for (var i = 0; i < originalLength; i++) {
        updated = updateKeys(original[i]);
        updatedArray.push(updated);
        updated = {};
      }

      final = updatedArray;
    } else {
      updated = updateKeys(original);
      final = updated;
    }

    return updated;

    function updateKeys(original) {
      for (var p in original) {
        if (original.hasOwnProperty(p)) {

          // result += p + " , " + original[p] + "\n";
          if (typeof original[p] === "object") {
            if (original[p] != null) {
              let uk = new UpdateKeys(original[p]);
              original[p] = uk.snakeCase();
            }
          }
          updated[changeCase.snakeCase(p)] = original[p];
        }
      }

      return updated;
    }
  };

  this.camelCase = function() {
    //let final;
    let updated = {};
    let updatedArray = [];
    var original = this.myPayload;

    if (Array.isArray(original)) {
      let originalLength = original.length;

      for (var i = 0; i < originalLength; i++) {
        updated = updateKeys(original[i]);
        updatedArray.push(updated);
        updated = {};
      }

      return updatedArray;
    } else {
      updated = updateKeys(original);
      return updated;
    }

    function updateKeys(original) {
      for (var p in original) {
        if (original.hasOwnProperty(p)) {
          // result += p + " , " + original[p] + "\n";
          if ((typeof original[p] === "object") && !(original[p] instanceof Date)) {
            if (original[p] != null) {
              let uk = new UpdateKeys(original[p]);
              original[p] = uk.camelCase();
            }
          }
          updated[changeCase.camelCase(p)] = original[p];
        }
      }

      return updated;
    }
  };
}

module.exports = {
  UpdateKeys
};
