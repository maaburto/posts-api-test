'use strict';

let allowedMethods = '', allowedHeaders  = '';

function HTTP(cors) {
  allowedMethods = cors.methods.join();
  allowedHeaders = cors.headers.join();
}

function AddHeaderOptions(req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', allowedMethods);
  res['setHeader']('Access-Control-Allow-Headers', allowedHeaders);
  res.sendStatus(200);
}

function addHeaders(req, res, next) {
  res['setHeader']('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res['setHeader']('Access-Control-Allow-Methods', allowedMethods);

  // Request headers you wish to allow
  res['setHeader']('Access-Control-Allow-Headers', allowedHeaders);

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res['setHeader']('Access-Control-Allow-Credentials', true);

  if ('OPTIONS' === req.method) {
    res.sendStatus(200);
  }

  // Pass to next layer of middleware
  next();
}

function CustomErrors(err, req, res, next) {
  let status = err.status || 500;  

  if (process['env']['NODE_ENV'] === 'prod') {
    res.status(status).send({
      message: err.message,
      code: err.code
    });
  } else {
    res.status(status).send({
      message: err.message,
      code: err.code,
      detail: err.detail,
      severity: err.severity,
      error: err.stack
    });
  }

  console.error(err.stack);
}

function NotFound(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
}

function MorganLogger(env) {
  let log = null;

  switch (env.toLowerCase()) {
    case 'prod':
      log = 'combined';
      break;
    default:
      log = 'dev';
      break;
  }

  return log;
}

HTTP.prototype = { AddHeaderOptions, addHeaders, CustomErrors, NotFound, MorganLogger};
module.exports = { HTTP };
