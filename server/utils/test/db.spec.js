const chai = require('chai');
const { instance } = require('../db');
const uriFake = 'mongodb://localhost:27017/dbfake';

beforeAll(async () => {
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('db utility suite', () => {
  it('Should return an object containing mongoose connection', () => {
    const { base } = instance(uriFake);
    base.should.be.a('object');
  });

  it('Should to be present a \"model\" property', () => {
    const { base } = instance(uriFake);
    base.should.have.property('model');  
  });

  it('Should to be present a \"Schema\" property', () => {
    const { base } = instance(uriFake);
    base.should.have.property('Schema');  
  });
});
