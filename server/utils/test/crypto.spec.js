const chai = require('chai');
const jwt = require('jsonwebtoken');
const {
  hashPassword,
  comparePassword,
  sign,
  verify
} = require('../crypto');
const salt = 10;
const password = 'password123';
let hash = null;

beforeAll(async () => {
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
  hash = await hashPassword(salt, password);
});

describe('crypto utility suite', () => {
  it('Should generate an appropriate hash in base 10 salt', () => {
    hash.should.be.a('string');
    hash.should.include('$2b$10');
  });

  it('Should compare a password and return true', async () => {
    const passwordCompared = await comparePassword(password, hash);
    passwordCompared.should.be.a('boolean');
    passwordCompared.should.be.equal(true);
  });

  it('Should compare a password and return false', async () => {
    const passwordCompared = await comparePassword(password, hash + '==');
    passwordCompared.should.be.a('boolean');
    passwordCompared.should.be.equal(false);
  });

  it('Should sign a JWT token and get an object decoded', async () => {
    const token = await sign('username', 'superSecretKey', '1 day');
    const contentDecoded = jwt.decode(token);

    contentDecoded.should.have.any.keys(['userId', 'iat', 'exp']);
  });

  it('Should get an object decoded and get a username value at userId key', async () => {
    const superSecretKey = 'superSecretKey';
    const username = 'username';
    const token = await sign(username, superSecretKey, '1 day');
    const decoded = await verify(token, superSecretKey);

    decoded.should.have.property('userId').that.be.equal(username);
  });
});
