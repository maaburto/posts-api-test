const chai = require('chai');
const UpdateKeys = require('../objectHelper').UpdateKeys;
let objectHelper = null;

beforeAll(async () => {
  global.payload = {
    'user_id': 1,
    'first_name': 'Jhon',
    'last_name': 'Doe'
  };

  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('objectHelper utility suite', () => {
  it('Should returns input payload', () => {
    objectHelper = new UpdateKeys(payload);
    objectHelper.myPayload.should.be.eql(payload);
  });

  it('Should returns camelCase payload', () => {
    objectHelper = new UpdateKeys(payload);
    const assertKeys = ['userId', 'firstName', 'lastName'];

    const camel = objectHelper.camelCase();
    const keys = Object.keys(camel);

    keys.should.be.eql(assertKeys);
  });
});
