const chai = require('chai');
const allowedEndpoints = require('../allowedEndpoints');

beforeAll(async () => {
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('allowing endpoints utility suite', () => {
  it('Should return false to skip next checking middleware', () => {
    const publicEndpoint = allowedEndpoints('/posts', 'POST');
    publicEndpoint.should.be.a('BOOLEAN');
    publicEndpoint.should.be.equal(false);
  });

  it('Should return true to expose a public endpoint', () => {
    const publicEndpoint = allowedEndpoints('/users', 'POST');
    publicEndpoint.should.be.a('BOOLEAN');
    publicEndpoint.should.be.equal(true);
  });
});
