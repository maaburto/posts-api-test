const chai = require('chai');
const sinon = require('sinon');
const HTTP = require('../http').HTTP;
const cors = {
  'methods': ['OPTIONS', 'GET', 'POST', 'PUT', 'DELETE', 'PATCH'],
  'headers': [
    'content-type',
    'Content-Type',
    'Authorization',
    'x-access-token',
    'x-reset-token',
    'x-auto-token',
    'x-access-with'
  ]
};
let request = () => {};
let response = null;
let next = null;

beforeAll(() => {
  global.jestExpect = global.expect;
  global.expect = chai.expect;
  chai.Should();
});

describe('http utility suite', () => {
  beforeEach(() => {
    next = sinon.spy();
    response = {
      header: sinon.spy(),
      setHeader: sinon.spy(),
      sendStatus : sinon.spy(),
    };
  });

  it('Should AddHeaderOptions function have called to header method', () => {
    const httpUtils = new HTTP(cors);
    httpUtils.AddHeaderOptions(request, response);

    response.header.called.should.be.equal(true);
    response.header.callCount.should.be.equal(2);
  });

  it('Should AddHeaderOptions function have called to setHeader method', () => {
    const httpUtils = new HTTP(cors);
    httpUtils.AddHeaderOptions(request, response);

    response.setHeader.called.should.be.equal(true);
    response.setHeader.callCount.should.be.equal(1);
  });

  it('Should AddHeaderOptions function have called to sendStatus method', () => {
    const httpUtils = new HTTP(cors);
    httpUtils.AddHeaderOptions(request, response);

    response.sendStatus.called.should.be.equal(true);
    response.sendStatus.callCount.should.be.equal(1);
    response.sendStatus.calledWith(200).should.be.equal(true);
  });

  it('Should addHeaders function have called to setHeader method', () => {
    const httpUtils = new HTTP(cors);
    httpUtils.addHeaders(request, response, next);

    response.setHeader.called.should.be.equal(true);
    response.setHeader.callCount.should.be.equal(4);
  });

  it('Should addHeaders function not call to sendStatus method', () => {
    const httpUtils = new HTTP(cors);
    httpUtils.addHeaders(request, response, next);

    response.sendStatus.called.should.be.equal(false);
  });

  it('Should addHeaders function call to sendStatus method', () => {
    const httpUtils = new HTTP(cors);
    httpUtils.addHeaders({
      method: 'OPTIONS'
    }, response, next);

    response.sendStatus.called.should.be.equal(true);
  });

  it('Should addHeaders function call to next method to jump over next middleware', () => {
    const httpUtils = new HTTP(cors);
    httpUtils.addHeaders(request, response, next);

    next.called.should.be.equal(true);
  });

  it('Should MorganLogger function returns \"combined\" log', () => {
    const httpUtils = new HTTP(cors);
    const log = httpUtils.MorganLogger('prod');

    log.should.be.a('string');
    log.should.be.equal('combined');
  });

  it('Should MorganLogger function returns \"dev\" log', () => {
    const httpUtils = new HTTP(cors);
    const log = httpUtils.MorganLogger('qa');

    log.should.be.a('string');
    log.should.be.equal('dev');
  });

  afterEach(() => {
    sinon.restore();
  });
});

