'use strict';

module.exports = allowedEndpoints;

function allowedEndpoints(url, method) {
  const _url = url.split('/');

  if (_url[1] === 'users' && method === 'POST') {
    return true;
  }

  return false;
}

const isAlphaNumeric = (ch) => {
  return ch.match(/^[a-z0-9]+$/i) !== null;
};

const isAlpha = (ch) => {
  return ch.match(/^[a-z]+$/i) !== null;
};
