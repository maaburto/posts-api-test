# POST API TEST 

Installation
-------------
```bash
npm install
```

Starting up
-------------
To start up application you should make following:
```bash
npm start
```
you can listen up REST API at 4597 port (By Default)

Debugging
--------------
To start up application you should make following:
```bash
npm run debug
```
Note: You can listen up inspection at 9437 port

Test
-------------
```bash
npm run test
```

Usage
------

POST
/signup
```bash
{
  "name": "Jhon",
  "email": "Jhon.Doe@blade.com",
  "password": "password"
}
```

POST
/login
```bash
{
	"email":"Jhon@blade.com",
	"password": "password"
}
```

GET
* /post
* /post/:id

POST
/posts
```bash
{
	"title": "Post title",
	"imageUrl": "https://www.url.com",
	"content": "Content 123",
	"creator": "5dd05cef5d8e950100617cf6"
}
```

PUT
/posts/:id
```bash
{
	"title": "Title updated",
	"imageUrl": "https://www.url-123.com",
	"content": "My Content",
	"creator": "5dd05cef5d8e950100617cf6"
}
```

DELETE
* /posts/:id

