'use strict';

const express = require('express');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const { UpdateKeys } = require('./server/utils/objectHelper');
const Routes = require('./server/routes/index').Routes;
const { secret, port, env, cors } = require('./config'); // get our config file
const HTTP = require('./server/utils/http').HTTP;
const JSONPackage = require('./package');

let app = module.exports = express();
const apiUrl = '/';

const httpUtils = new HTTP(cors);
function customJson(obj) {
  let helper = new UpdateKeys(obj);
  return this.send(helper.camelCase());
}
express.response.customJson = customJson;

// Set env defaults dev
app['set']('env', env);
app['set']('superSecret', secret); // secret variable
app['set']('port', port); // port
app['enable']('trust proxy');

app['options']("/*", httpUtils.AddHeaderOptions);
// Add headers
app['use'](httpUtils.addHeaders);
app['use'](morgan(httpUtils.MorganLogger(env)));
app['use'](cookieParser());
app['use'](bodyParser.json({ limit: '10mb' }));
app['use'](bodyParser.urlencoded({ extended: false }));
// app.use(passport.initialize());

// routes
const routes = new Routes(JSONPackage);
app['use'](routes.indexRoute());
app['use'](apiUrl, routes.route());

// catch 404 and forward to error handler
app['use'](httpUtils.NotFound);

// error handlers
// no stacktraces leaked to user
app['use'](httpUtils.CustomErrors);

module.exports = app;
