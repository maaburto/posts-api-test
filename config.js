'use strict';

const config = {
  'env': process['env']['NODE_ENV'] || 'dev'
  ,'port': getEnv('PORT') || 4597
  ,'secret': getEnv('SECRET') || 'DCV8xuScYuPBKJn6SOEY84Wb+ylHg1RfAnwrWc+DhAwntMVjvtn3JtXuYABMaV/r'
  ,'token': getEnv('TOKEN') || '1c885004-434f-470d-a3f4-307a90e8ec73'
  ,'apiUserId': '7d36ae4a-d546-4c39-b14a-65bf47bd32d5'
  ,'jwtExpire': '7 days'
  ,'saltFactor': 10
  ,'cors': {
    'methods': ['OPTIONS', 'GET', 'POST', 'PUT', 'DELETE', 'PATCH'],
    'headers': ['content-type', 'Content-Type', 'Authorization', 'x-access-token',
      'x-reset-token', 'x-auto-token', 'x-access-with']
  }
  ,'connectionString': getEnv('DATABASE_URL') || 'mongodb://localhost:27017/posts'
};

function getEnv(variable) {
  return process['env'][variable];
}

module.exports = config;
